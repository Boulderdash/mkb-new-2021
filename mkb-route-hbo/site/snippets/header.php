<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= $site->title() ?>, <?= $page->title() ?></title>

        <?= css([
        'assets/css/style.css'
        ]) ?>

        <?= js([
        'assets/js/jquery.min.js',
        'assets/js/lightslider.min.js',
        'assets/js/lightgallery.min.js',
        'assets/js/jquery.chocolate.js',
        'assets/js/swiper.min.js'
        // 'assets/js/copyright.js'
        ]) ?>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css2?family=PT+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">


        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&family=Source+Serif+Pro:ital,wght@0,400;0,700;1,200&display=swap" rel="stylesheet">


        
    </head>
<body>



<?php snippet('backtotop') ?>






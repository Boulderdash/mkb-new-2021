
<section>
    
    <ul class="lightslider quotes">
   
            <?php $items = $page->addresses()->toStructure(); ?>
            <?php foreach ($items as $item): ?>
            <li>
     
                <h1><?= $item->titel()->text() ?></h1>
                <p><?= $item->omschrijving()->kirbytext() ?></p>
                <em><?= $item->afzender()->text() ?></em>

            </li>

            <?php endforeach ?>

   
    </ul>

</section>    


<?php if ($page->kopenintro()->isNotEmpty()): ?>
        <section style="background-color: <?= $page->bground() ?>">
            <?= $page->kopenintro()->kirbytext() ?>
        </section>
<?php endif ?>

<section style="background-color: <?= $page->bground() ?>">

    <div class="video-container">

    <iframe src="https://player.vimeo.com/video/<?= $page->videolink() ?>" width="640" height="352" frameborder="0" allowfullscreen></iframe>
    </div>
    
</section>    


<?php if ($page->lijn()->isNotEmpty()): ?>
    <section>
        <hr>
    </section>
<?php endif ?>   
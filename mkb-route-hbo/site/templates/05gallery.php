

<section>
    
        <div id="lightgallery" class="gridgallery">
            <?php foreach($page->images() as $image): ?>
                <a href="<?= $image->url() ?>">
                    <figure>
                        <img src="<?= $image->focusCrop(400,250)->url() ?>">
                    </figure>
                </a>
            <?php endforeach ?>
        </div>

</section>    

<?php foreach($page->images() as $image): ?>

        <figure>
                <img src="<?= $image->url() ?>">
        </figure>

<?php endforeach ?>
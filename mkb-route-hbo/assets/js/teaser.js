// const pakDatumvanpagina = document.getElementById('onzichtbaar');

// let countDownDate = parseInt(pakDatumvanpagina.innerHTML) * 1000;

let countDownDate = new Date("Nov 7, 2019 12:00:00").getTime();
// let countDownDate = new Date("Jan 5, 2021 15:37:25").getTime()

let daysContainer = document.getElementById('days');
let hoursContainer = document.getElementById('hours');
let minutesContainer = document.getElementById('minutes');
let secondsContainer = document.getElementById('seconds');

var x = setInterval(function() {

  var now = new Date().getTime();

  var distance = countDownDate - now;

  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  daysContainer.innerHTML = days;
  hoursContainer.innerHTML = hours;
  minutesContainer.innerHTML = minutes;
  secondsContainer.innerHTML = seconds;


  if (distance < 0) {
    clearInterval(x);
    document.getElementById("titel").innerHTML = "E-zine gaat nu online";
  }
}, 1000);

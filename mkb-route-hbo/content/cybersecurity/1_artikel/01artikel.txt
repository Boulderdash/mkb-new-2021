Title: artikel

----

Volledig:

#### Ad Cyber Security start met 125 studenten 
# Zoeken naar beveiligingslekken 

(image: cyber-security.jpg alt: foto Cybersecurity)

**De Hogeschool van Amsterdam en NLdigital nemen sinds september 2021 deel aan het programma 'Mkb-route in het hbo' met de opleiding Cyber Security. Plan is daar volgend jaar de opleiding Software Development aan toe te voegen. Studenten kunnen in twee jaar tijd een Associate degree (Ad) halen. Beide opleidingen zijn bedoeld voor leerlingen met een vooropleiding mbo, havo of vwo.**

De praktijkgerichte niveau 5 opleiding Cyber Security draait feitelijk al twee jaar, maar haakt vanaf september voor het eerst ook aan bij het landelijke programma ‘Mkb-route in het hbo’. Studenten die deelnemen aan de opleiding krijgen twee dagen per week les, en werken twee dagen per week aan echte opdrachten van echte mkb-bedrijven in het projectenlaboratorium van de opleiding, het Cyber Security Lab. Zo testen zij bijvoorbeeld de veiligheid van ict-systemen. Als ethische hackers zoeken zij zwakke punten in de cyber security van menig ondernemer.  

De eerste studenten hebben inmiddels de opleiding al met een diploma en een baan verlaten.  Zij kunnen aan de slag als ethical hacker, pentester of threathunter bij bedrijven of organisaties uit verschillende sectoren. 

### Duurzame relatie 

Volgens Reza Esmail, associate professor Cybersecurity aan de Hogeschool van Amsterdam, is er nog even sprake van een suboptimale situatie: “Het liefst hebben we natuurlijk dat leerlingen bij ondernemers in huis aan de slag gaan, maar de coronapandemie heeft dat gefrustreerd. Als tussenvorm werken studenten daarom in ons laboratorium, maar wel aan echte opdrachten vanuit het bedrijfsleven. Heeft een mkb-bedrijf wel ruimte om onze student fysiek op te nemen, dan gaan we dat uiteraard meteen regelen.” 

 

De HvA wil deze pilot gebruiken om te verkennen of een structurele publiek-private samenwerking met het mkb mogelijk is, om vervolgens samen de onderwijsinhoud bijdetijds te maken en te houden. Reza: “Doel is dat zoveel mogelijk studenten met echte vragen vanuit het bedrijfsleven in aanraking komen. Via een verkort traject (Ad) waarin leren en werken gecombineerd worden, met eigentijds en praktisch onderwijs, waar vakmanschap centraal staat en waarbij deelnemers een volwaardig diploma kunnen behalen.” 

 

Om ook voldoende opdrachten voor studenten in huis te halen, maakt de HvA gebruik van een private partij. Die gaat op zoek naar passende opdrachtgevers. Reza: “Zodra een bedrijf een mogelijke opdracht heeft, koppelen we die ondernemer direct aan ons netwerk. Doel is dat we uiteindelijk wel samen een duurzame relatie opbouwen.” 


### Waardevolle opleiding 

Via de mkb-route in het hbo is er sprake van een win-win-win situatie, denkt hij. “Bedrijven hebben een tekort aan relevant geschoolde professionals en hebben de mogelijkheid om hun vacatures beter te vervullen. Studenten doen op de werkvloer vakkennis en ervaring op en hebben na hun studie zicht op een goede carrière. En de hogeschool bevestigt zijn positie als kennisinstelling. Via wat we onze studenten leren kunnen we onze kennis bovendien terugbrengen naar mkb-bedrijven in onze regio.”  

De Ad is dit jaar gestart met maar liefst 125 nieuwe eerstejaars die vanaf dag 1 met ‘echte’ opdrachten aan de slag gaan. Reza: “We merken dat deze Ad vooral voor studenten van het mbo een waardevolle opleiding is. Met name voor die studenten die niet snel een bachelor aandurven. En vergeet niet: we leiden deze studenten ook daadwerkelijk op voor een baan. Want wie dat wil kan daarna bijna zonder uitzondering direct bij een werkgever aan de bak.” 

---

## Over deze mkb-route in het hbo 

(video: https://vimeo.com/485639433/29c4ca8b88)

---

#### Louis Spaninks, NLdigital 
### ‘Ad’s zijn de missing link’ 

<div class="vet">
<p>NLdigital is zeer content met de Ad Cyber Security en de voorgenomen start van de Ad Software Development van de HvA. “Zeker nu digitalisering steeds belangrijker wordt voor onze economische groei is dit heel belangrijk”, stelt Louis Spanink.</p>
</div>

NLdigital is een collectief van ruim 600 bedrijven die de digitale transformatie mogelijk maken. Het collectief vertegenwoordigt wereldwijde spelers én honderden scale-ups en mkb’ers die gezamenlijk de basis vormen van digitaal Nederland. Louis: “Voor ons is het van belang om opleidingsinitiatieven te ondersteunen die ertoe doen. Voor deze mkb-leerroutes is dat wat ons betreft zeker aan de orde. Deze Ad’s vormen namelijk de verbinding vormt tussen MBO en HBO, de ‘missing link’.” 

<div class="kader lichtblauw">

### HvA-hackers in de media 

Aan belangstelling heeft de Ad Cyver Security van de HvA en NLdigital bepaald geen gebrek. Zo maakte EenVandaag op 2 augustus een (link: https://eenvandaag.avrotros.nl/item/het-digitale-leger-van-de-toekomst-deze-studenten-leren-hacken-om-juist-te-beschermen-tegen-cyberaanvallen/  text: reportage) over deze opleiding.

<div class="avrocontainer">
<iframe class="videoavro" src="https://eenvandaag.avrotros.nl/embed/529214/" width="560" height="315" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
</div>

<style>
.avrocontainer {
    position: relative;
    padding-bottom: 56.10%;
    height: 0;
    overflow: hidden;
}
.videoavro {
position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
</style>


Ook Het Parool pakte in juni uit, met een fors artikel waarin studenten vertellen over hun opleiding: ‘Ik loop nu voor een project mee met een bedrijf waar we pagina’s vol inloggegevens hebben gevonden. Als je wilt, zou je die binnen vijf minuten kunnen vinden op internet, terwijl dat helemaal niet de bedoeling is.’ 

(link: https://www.parool.nl/ps/zij-worden-opgeleid-tot-ethische-hackers-mensen-hebben-te-veel-vertrouwen-in-elkaar~bc9de643/ 
 text: Lees hier het hele artikel) 

</div>

<div class="kader lichtblauw">
 
## Meer weten? 

https://www.hva.nl/opleiding/cyber-security-ad/cyber-security.html 
https://www.hva.nl/opleiding/cyber-security-ad/studieprogramma/studieprogramma.html 
https://www.hva.nl/opleiding/cyber-security-ad/praktische-informatie/praktische-informatie.html


</div>

----

Bground: 

----

Lijn: 
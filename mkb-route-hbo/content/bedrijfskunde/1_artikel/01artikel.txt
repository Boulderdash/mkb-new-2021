Title: artikel

----

Volledig:

#### Ad Technische Bedrijfskunde 
# Oplossing voor afvallers vergroot instroom techniek 

(image: metaalsector.jpg alt: plaatje van metaalsector)

**Dit studiejaar zijn de eerste deelnemers aan Hogeschool Windesheim gestart met hun mkb-route in het hbo. Een belangrijke mijlpaal voor Gerwin Peppelman van OpleidingsBedrijfMetaal Oost die de afgelopen jaar de trekker was van de pilot HBO Doorstroomtraject Metaal. In samenwerking met Hogeschool Windesheim heeft dit geresulteerd in een nieuwe Ad-opleiding Technische Bedrijfskunde met het profiel industriële automatisering.**

 Zo’n twintig procent van het bruto nationaal product komt landelijk gezien voor rekening van de maakindustrie en in de regio Oost ligt dit percentage zelfs wat hoger. De metaalsector neemt daarvan een heel groot deel voor rekening. Maar al jaren heeft de sector behoefte aan meer instroom van nieuwe werknemers. De gemiddelde leeftijd in de sector is bovendien hoog, vertelt Gerwin. “In het mkb metaal is dat 42 jaar en in de industrie zelfs 45.”  

Zonder voldoende instroom loopt de effectiviteit in de sector een risico en komt de continuïteit onder druk te liggen. Outsourcen is ook geen optie want binnen het mkb metaal is men erop gericht dat veel metaalwerkzaamheden in Nederland gedaan moet worden. Dat is beroepstrots. En ze hebben ook wel een punt: Nederland is er echt goed in.  

### Acht uur naar school 
Studenten gaan die de nieuwe Ad-opleiding volgen gaan acht uur per week naar school, om precies te zijn een middag en een avond. Ze volgen dan verschillende modules van bestaande studies. De rest van de week zijn ze bij een mkb-metaalbedrijf aan het werk. Gerwin: “De leeruitkomsten zijn puur gericht op wat je op de werkvloer toepast. Hoe optimaliseer je de werkprocessen. Dat soort trajecten worden per casus in de opleiding uitgewerkt.” 

De theoretische kennislessen van de tweejarige studie volgen de studenten klassikaal. Lessen, praktijktrainingen en coaching worden uitgevoerd in het T-gebouw van Windesheim in Zwolle en bij Perron 038 bij station Zwolle. 


(image: gerwin.png alt: foto gerwin)

### Tussenoplossing voor uitvallers 
De directe aanleiding voor deze opleiding vormde een gesprek dat Gerwin had, samen met de scholingspartner, Deltion College. Binnen het onderwijs waren signalen dat een groot deel van de havisten uitviel in het hbo. Navraag bij Hogeschool Windesheim leverde op dat het niet alleen havisten waren die uitvielen, maar ook vwo’ers. Van de 435 nieuwe techniekstudenten vielen er 155 af in het eerste jaar, zo’n 36 procent. Gerwin: “Hbo-instellingen switchen deze studenten naar andere studies. De weg naar het mbo vinden studenten en instellingen geen optie. Dat is afstromen, en studenten vinden het vaak ook fijn om in hun sociale kring op het hbo te blijven. Maar het zijn dus allemaal studenten die in potentie de techniek in willen en daar uiteindelijk niet terechtkomen.”  

Een tussenoplossing kon uitkomst bieden. Windesheim zag wel iets in een AD-opleiding, voor de afvallers in het reguliere hbo en voor de mbo-4 mensen. En dat was de opening die OBM-Oost nodig had. Gerwin: “Dat Windesheim aan boord kwam, was echt fijn. Dat we met de opleiding als pilot konden meedraaien in het programma ‘Mkb-route in het hbo’ heeft daar zeker ook toe bijgedragen. Dat netwerk is sterk en groot en zet zich formeel en informeel in om die mkb-route in het hbo voor elkaar te krijgen.” Ook de bijeenkomsten met vertegenwoordigers van andere pilots in het land waren leerzaam. Gerwin vindt het interessant om te horen hoe anderen het aanpakken. “Daar steek je altijd weer wat van op.” 

### Combinatie van twee studies 
OBM-Oost heeft de mkb-ondernemers via een enquête vooraf gevraagd waar zij behoefte aan hadden (zie kader). Zo ontdekten ze dat door technische bedrijfskunde en robotica te combineren precies de opleiding ontstond die nodig is voor het mkb. “Dat was een groot voordeel. Als er behoefte was geweest aan een compleet nieuwe opleiding dan waren de ontwikkelkosten een stuk hoger en hadden we minimaal vijftien studenten nodig gehad voordat we konden starten. Door modules van bestaande opleidingen te combineren konden we al van start met een minimaal aantal studenten.”  

---

## Over deze mkb-route in het hbo 

(video: https://vimeo.com/485643203/b007ef718a)

---

## OBM Oost partner van de mkb-route 
OpleidingsBedrijfMetaal Oost (OBM Oost) is een samenwerkingsverband van met name mkb-bedrijven in de metaal- en maakindustrie. OBM Oost werft studenten voor de ongeveer 250 (leer)bedrijven waarmee wordt samengewerkt. Door een goede match te zoeken tussen student en leerbedrijf en voortgangsgesprekken te voeren wordt het slagingspercentage zeer positief beïnvloed. Daarnaast organiseert OBM Oost scholing en cursussen waar in het bedrijfsleven behoefte aan bestaat. Vanuit die rol heeft OBM Oost deelname aan het programma ‘Mkb-route in het hbo’ opgepakt.  
 
---

## Enquête: tweedeling ontstaat in metaalsector  
Om te weten waar het mkb in de metaalsector echt behoefte aan heeft stuurde OBM OOST een enquête naar 430 mkb-bedrijven. Uit de ruim 250 antwoorden blijkt dat er een tweedeling ontstaat in de sector. Een deel van de bedrijven blijft heel traditioneel werken. Een ander deel moderniseert in rap tempo en schuift op naar automatisering en robotica. Meer dan 68 procent van de respondenten gaf aan dat automatiseringen en robotica invloed krijgt op het functioneren van medewerkers. Daarmee ontstaan ook andere scholingsbehoeftes. Bedrijven geven aan meer scholing te willen in efficiënt omgaan met tijd, materiaal en geld, slim produceren, robotprogrammeren en werken met ERP systemen. 

---

## Dáárom mkb-route in het hbo 

(image: frank.png alt: foto frank huising)

### Frank Huising, docent technische bedrijfskunde Windesheim:  
“Je leert als student binnen deze opleiding bedrijfskundige processen, gericht op automatisering. Maar je leert ook de technische kant. Je leert programmeren van robots, cobots en plc’s.” 

(image: auke.png class: kleinlinks alt: foto auke sjoerd Tolsma)

### Auke Sjoerd Tolsma, directeur Polder Staalproducties en voorzitter districtsbestuur regio Oost van de Koninklijke MetaalUnie:  
“We willen graag dat machines een deel van het werk overnemen. Maar wil je dat goed doen, dan moet je dat op een ander abstractieniveau oppakken. Wij zijn op zoek naar een studie die aansluit bij mensen die graag met hun handen willen werken maar die ook heel goed na kunnen denken. Die combinatie, die zien wij in niveau 5.” 

---

<div class="kader lichtblauw">

## Meer weten? 
Opleiding: (link: www.deleukstemetaalopleiding.nl text: www.deleukstemetaalopleiding.nl)
Promotiefilmpje: https://www.youtube.com/watch?v=9gSRylZQBvQ

</div>

----

Bground: 

----

Lijn: 